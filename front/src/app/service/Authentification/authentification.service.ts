import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { tap } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class AuthentificationService {

  private url = "http://localhost::8080/api/login_check";

  user = new BehaviorSubject<boolean>(false);

  constructor(private Http:HttpClient) { 
    if(localStorage.getItem('token')) {
      this.user.next(true);
    }
  }

  login(username:string, password:string): Observable<any> {
    return this.Http.post<any>(this.url, {
      username:username,
      password:password
    })
    .pipe(
        tap(response => {
        if(response.token){
          localStorage.setItem('token',response.token);
          this.user.next(true);
          console.error(this.user);
          
        }
      })
    );
   }

   logout(){
     localStorage.removeItem('token');
     this.user.next(false);
   }

   isLoggedIn():boolean {
    if(localStorage.getItem('token')){
      return true;
    }
    return false;
   }
}
