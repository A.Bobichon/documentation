import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Category } from '../../entity/category';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  private url = "http://localhost:8080/apiCategory/"

  constructor(private Http:HttpClient) { }

  findAllCat():Observable<Category[]>{
    return this.Http.get<Category[]>(this.url);
  }

  findOneCat(category:Category):Observable<Category>{
    return this.Http.get<Category>(this.url + category.id);
  }

  addCat(category:Category):Observable<Category>{
    return this.Http.post<Category>(this.url, category);
  }

  delete(category:Category){
    return this.Http.delete<Category>(this.url + category.id);
  }
}
