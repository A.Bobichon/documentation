<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Documentation;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @Route("/apiDocumentaion", name="api_documentation")
 */
class ApiDocumentationController extends AbstractController
{
    /**
     * @Route("/", methods="GET")
     */
    public function findAllDoc()
    {
        $documentation = $this->getDoctrine()
            ->getRepository(Documentation::class)
            ->findAll();

        $serializer = $this->get('jms_serializer');

        return JsonResponse::fromJsonString(
            $serializer->serialize($documentation, 'json')
        );
    }

    /**
     * @Route("/{id}", methods="GET")
     */
    public function findOneDoc(Documentation $doc)
    {
        $serializer = $this->get('jms_serializer');

        return JsonResponse::fromJsonString(
            $serializer->serialize($doc, 'json')
        );
    }

    /**
     * @Route("/", methods="POST")
     */
    public function addDoc(Request $req)
    {

        $serializer = $this->get('jms_serializer');
        $manager = $this->getDoctrine()->getManager();

        $doc = $serializer->deserialize(
            $req->getContent(),
            Documentation::class,
            "json"
        );


        $manager->persist($doc);
        $manager->flush();

        $json = $serializer->serialize($doc, "json");

        return JsonResponse::fromJsonString($json, 201);
    }

    /**
     * @Route("/{id}", methods="DELETE")
     */
    public function delete(Documentation $doc){
        $manager = $this->getDoctrine()->getManager();
        $manager->remove($doc);
        $manager->flush();

        return new JsonResponse([], 204);
    }


}
