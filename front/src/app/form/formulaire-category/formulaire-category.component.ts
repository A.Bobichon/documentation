import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import { CategoryService } from '../../service/serviceEntity/category.service';


Component({
  selector: 'app-formulaire-category',
  templateUrl: './formulaire-category.component.html',
  styleUrls: ['./formulaire-category.component.css']
});

export class FormulaireCategoryComponent implements OnInit {

  adForm:FormGroup;

  constructor(private formBuilder:FormBuilder ,private service:CategoryService) {

  this.initForm();

  }

  initForm(){
    this.adForm = this.formBuilder.group({
      name:["", Validators.required]
    });
  }

  onSubmitForm(){
    const value = this.adForm.value;
    this.service.addCat(value).subscribe( () => console.log("succesfull!"), data => console.log(data));
  }

  ngOnInit() {
  }



}
