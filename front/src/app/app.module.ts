import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RouterModule , Routes } from '@angular/router';
import { NavComponent } from './nav/nav.component';

import { HttpClientModule } from '@angular/common/http';
import { FormulaireCategoryComponent } from './form/formulaire-category/formulaire-category.component';



const routes:Routes = [
  { path: '' , redirectTo: '', pathMatch: 'full'},
  { path: 'formCat', component: FormulaireCategoryComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    FormulaireCategoryComponent //component
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    RouterModule.forRoot(
      routes,
      {enableTracing: true}
    ),
    HttpClientModule,

    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
