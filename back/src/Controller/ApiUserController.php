<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Category;

/**
 * @Route("/apiUser", name="api_user")
 */
class ApiCategoryController extends AbstractController
{

    /**
     * @Route("/", methods="POST")
     */
    public function addUser(Request $req,User $user,UserPasswordEncoderInterface $encoder)
    {

        $serializer = $this->get('jms_serializer');
        $manager = $this->getDoctrine()->getManager();

        $user = $serializer->deserialize(
            $req->getContent(),
            User::class,
            "json"
        );

        $user->setPassword($encoder->encodePassword($user, $user->getPassword()));
        $manager->persist($user);
        $manager->flush();

        $json = $serializer->serialize($user, "json");

        return JsonResponse::fromJsonString($json, 201);
    }


}