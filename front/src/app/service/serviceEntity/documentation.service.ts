import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Documentation } from '../../entity/documentation';


@Injectable({
  providedIn: 'root'
})
export class DocumentationService {

  private url = "http://localhost:8080/apiDocumentaion/";

  constructor( private Http:HttpClient ) { }

  findAllDoc():Observable<Documentation[]>{
    return this.Http.get<Documentation[]>(this.url);
  }

  findOneDoc(Documentation:Documentation):Observable<Documentation>{
    return this.Http.get<Documentation>(this.url + Documentation.id)
  }

  deleteDoc(Documentation:Documentation){
    return this.Http.delete<Documentation>(this.url);
  }
}
