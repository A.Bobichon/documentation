<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Category;

/**
 * @Route("/apiCategory", name="api_category")
 */
class ApiCategoryController extends AbstractController
{
    /**
     * @Route("/", methods="GET")
     */
    public function findAllCat()
    {
        $category = $this->getDoctrine()
            ->getRepository(Category::class)
            ->findAll();

        $serializer = $this->get('jms_serializer');

        return JsonResponse::fromJsonString(
            $serializer->serialize($category, 'json')
        );
    }

    /**
     * @Route("/{id}", methods="GET")
     */
    public function findOneCat(Category $category)
    {
        $serializer = $this->get('jms_serializer');

        return JsonResponse::fromJsonString(
            $serializer->serialize($category, 'json')
        );
    }

    /**
     * @Route("/", methods="POST")
     */
    public function addCat(Request $req)
    {

        $serializer = $this->get('jms_serializer');
        $manager = $this->getDoctrine()->getManager();

        $category = $serializer->deserialize(
            $req->getContent(),
            Category::class,
            "json"
        );


        $manager->persist($category);
        $manager->flush();

        $json = $serializer->serialize($category, "json");

        return JsonResponse::fromJsonString($json, 201);
    }

    /**
     * @Route("/{id}", methods="DELETE")
     */
    public function delete(Category $category){
        $manager = $this->getDoctrine()->getManager();
        $manager->remove($category);
        $manager->flush();

        return new JsonResponse([], 204);
    }


}
