import { Injectable } from '@angular/core';
import { AuthentificationService } from '../Authentification/authentification.service';
import { HttpRequest, HttpHandler, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';

import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor{

  constructor(private service:AuthentificationService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<any> {
    let token = localStorage.getItem('token');
    if(token) {
      let modifieRequest= req.clone({
        setHeaders: {
          Authorization: 'Bearer ' + token
        }
      });

      return next.handle(modifieRequest).pipe(
        catchError((err,caught) => {
          if(err.status === 401) {
            this.service.logout();
          }
          return throwError(err);
        })
      );
    }
    return next.handle(req);
  }
}